Install the package SQLAlchemy Migrate:

```
$ pip install alembic
```

The first time you must create a directory of a repository 'migrations' with
the versions:

```
$ alembic init migrations
```

Edit the migrations\env.py:

```
# import model.py
import os
import sys
MODEL_PATH = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..")
sys.path.append(MODEL_PATH)
import model


# edit this line and pass metadata
target_metadata = model.Base.metadata
```

Change in alembic.ini the database url:

```
sqlalchemy.url = postgresql+psycopg2://postgres:my_precious@localhost/flask_test
```

Now generate the first migrations and create the database:

```
alembic revision --autogenerate -m "initial"
```

This create the tables and the first migration in the dir migrations/versions.

To show the migrations use:

```
$ alembic history --verbose
Rev: 1cb5617eb93a (head)
Parent: <base>
Path: /media/ubuhome/juanfe/repos/flask_migrate/migrations/versions/1cb5617eb93a_initial.py

    initial

    Revision ID: 1cb5617eb93a
    Revises:
    Create Date: 2019-05-02 14:01:03.891652
```

To migrate untill the most recent change use:

```
$ alembic upgrade head
```
